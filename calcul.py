# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 11:29:16 2021

@author: lucie
"""


### module de calcul
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.coordinates import SpectralCoord
from astropy import time
import L1_class as L1

#### flattening functions
from scipy.optimize import curve_fit
from scipy.interpolate import CubicSpline
from scipy.stats import linregress as linear

import numpy  as np
import pandas as pd

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.interpolate import CubicSpline
from scipy.stats import linregress as linear

import astropy.units as u
from astropy.constants import c as v_light

from astropy.modeling.models import Voigt1D,  Gaussian1D

from astropy.coordinates import SpectralCoord
from astropy.coordinates import EarthLocation
from astropy.coordinates import SkyCoord

from astropy import time
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord, FK5

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']


def mask_fct(I,f, LINES, mask_width = 50, display = False):
    """
    Applies a mask on the expected lines.

    Parameters
    ----------
    :param I: integrated flux on which to apply the mask.
    :type I: ndarray.
    :param f: frequencies corresponding to :param: `I`.
    :type f: ndarray, same lenght as :param: `I`.
    :param LINES: expected lines (within :param: f range).
    :type LINES: list of floats.
    :param mask_width: total width (in channels) of the mask around the expected line, defaults to 50 channels.
    :type mask_width: int, optional.
    :param display: plots the new masked array over the original array, defaults to False.
    :type display: bool, optional.
    :return: masked integrated flux, root mean square of the line-free integrated flux, mask applied on :param: `I`.
    :rtype rms: array, float, np.ma.mask.
    """
    ### masque sur les lignes
    n = len(f)
    half_width = mask_width//2
    mask = np.zeros(n)

    for line in LINES :
        ## abcisses entourant la raie
        f_raie  = line
        try :
            n_r_min = np.where(f < f_raie)[0][-1]
        except :
            n_r_min = 0
        try :
            n_r_max = np.where(f > f_raie)[0][0]
        except :
            n_r_max = n
        arr = mask[max(n_r_min - (half_width-1), 0):min(n_r_max + half_width,n)] 
        mask[max(n_r_min - (half_width-1), 0):min(n_r_max + half_width,n)] = np.ones(len(arr))

    ### evaluation du rms
    I_masked = np.ma.masked_array(I, mask = mask).filled(np.nan)
    rms = np.nanstd(I_masked)

    ### affichage
    if display == True :
        fig = plt.figure(figsize = (10,8))
        fig.suptitle(f'Observation : freq range = {np.nanmin(f): .5f} MHz to {np.nanmax(f) : .5f} MHz| RMS = {rms : .2e}')
        gs  = plt.GridSpec(3,1)
        ax0 = fig.add_subplot(gs[0])
        ax0.step(f,I)
        ax0.set_ylabel('Tau')
        ax0.set_title('Post treated subband PS')
        ax1 = fig.add_subplot(gs[1], sharex = ax0)
        ax1.plot(f,mask)
        ax1.set_title('Mask - width = {mask_width} channels')
        ax2 = fig.add_subplot(gs[2], sharex = ax0, sharey = ax0)
        ax2.step(f,I_masked)
        ax2.set_title('Line-free channels')
        ax2.set_xlabel('Frequency [MHz]')
        ax2.set_ylabel('Tau')
        plt.tight_layout()
        plt.show()
    return I_masked, rms, mask

def running_avg(signal, freq, width, display = False):
    """
    Computes the running average of a given signal.

    Parameters
    ----------
    :param signal: signal to rebin.
    :type signal: array.
    :param freq: frequency of given signal (x axis).
    :type freq: array (same lenght as signal).
    :param width: width of the rebin window.
    :type width: int.
    :param display: plots the rebinned signal over the original signal, defaults to False.
    :type display: bool, optional.
    :return: new signal array, new frequency array.
    :rtype: array, array.
    """
    I = signal
    nb = int(len(signal)/width)

    new_sig = []
    old_sig = list(signal.copy())

    while len(old_sig)>= nb :
        m = np.nanmean(old_sig[:nb])
        new_sig.append(m)
        old_sig[:nb] = []

    new_freq = list(freq[nb//2:-1:nb]) + [freq[len(freq)-nb:][nb//2]]
    
    new_len = min(len(new_sig), len(new_freq))
    
    new_sig, new_freq = new_sig[:new_len], new_freq[:new_len]

    if display == True :
        plt.plot(freq,signal, label = 'Original')
        plt.plot(new_freq,new_sig, 'o', label = 'Rebinned')
        plt.legend()
        plt.show()
    return new_sig, new_freq


def doppler_corrections(mean_time, ra = '23h23m24s', dec = '58d48m54',  obs_lat=47.367686, obs_lon=2.194313, obs_alt=150.):
    """
    computes the projected velocity of the telescope wrt four coordinate systems: 
    geo, helio, bary, lsr.
    To correct a velocity axis to the LSR do:
    vcorr = doppler_corrections(ra, dec, mean_time).
    rv = rv + vcorr[3] + rv * vcorr[3] / consts.c.
    where rv is the observed radial velocity.
    

    Parameters
    ----------
    :param ra: right ascension in degrees, J2000 frame.
    :param dec: declination in degrees, J2000 frame.
    :param mean_time: mean time of observation in isot format. Example "2017-01-15T01:59:58.99".
    :param obs_lon: East-longitude of observatory in degrees.
    :param obs_lat: Latitude of observatory in degrees.
    :param obs_alt: Altitude of the observatory in meters.
    
    :return: $\Delta_v$ to add to the expected velocity.
    :rtype: float.
    """
    # Initialize ra and dec of source.
    src = SkyCoord(ra, dec, frame='icrs', unit=u.deg)
    
    # Local properties.
    mytime = time.Time(mean_time, format='isot', scale='utc')
    location = EarthLocation.from_geodetic(lat=obs_lat*u.deg, lon=obs_lon*u.deg, height=obs_alt*u.m)
 
    # Orbital velocity of Earth with respect to the Sun.
    # helio = for source projected velocity of earth orbit with respect to the Sun center.
    # bary = for source projected velocity of earth + moon orbit with respect to the Sun center.
    barycorr = src.radial_velocity_correction(obstime=mytime, location=location)  
    barycorr = barycorr.to(u.km/u.s)
    heliocorr = src.radial_velocity_correction('heliocentric', obstime=mytime, location=location)  
    heliocorr = heliocorr.to(u.km/u.s)
        
    # Earth rotational velocity
    # Taken from chdoppler.pro, "Spherical Astronomy" R. Green p.270 
    lst = mytime.sidereal_time('apparent', obs_lon)
    obs_lat = obs_lat * u.deg
    obs_lon = obs_lon * u.deg
    hour_angle = lst - src.ra
    v_spin = -0.465 * np.cos( obs_lat ) * np.cos( src.dec ) * np.sin( hour_angle )

    # LSR defined as: Sun moves at 20.0 km/s toward RA=18.0h and dec=30.0d in 1900J coords
    # WRT objects near to us in Milky Way (not sun's rotation WRT to galactic center!)
    lsr_coord = SkyCoord( '18h', '30d', frame='fk5', equinox='J1900')
    lsr_coord = lsr_coord.transform_to(FK5(equinox='J2000'))

    lsr_comp = np.array([ np.cos(lsr_coord.dec.rad) * np.cos(lsr_coord.ra.rad), \
                          np.cos(lsr_coord.dec.rad) * np.sin(lsr_coord.ra.rad), \
                          np.sin(lsr_coord.dec.rad) ])

    src_comp = np.array([ np.cos(src.dec.rad) * np.cos(src.ra.rad), \
                          np.cos(src.dec.rad) * np.sin(src.ra.rad), \
                          np.sin(src.dec.rad) ])

    k = np.array( [lsr_comp[0]*src_comp[0], lsr_comp[1]*src_comp[1], lsr_comp[2]*src_comp[2]] )
    v_lsr = 20. * np.sum(k, axis=0) * u.km/u.s
    
    geo = - v_spin
    helio = heliocorr
    bary = barycorr
    lsr = barycorr + v_lsr
    vtotal = [geo, helio, bary, lsr]
    
    return vtotal



def doppler_correction(f,v):
    from astropy import constants as const
    c0 = const.c.value
    return f/(1+v/c0)

### processing function
def extract_lines(HDU, quantum_nb, f_range, sub_range):
    """
    Entry :
    path_fits : str   // path to the .spectra.fits data.
    f_range   : tuple // beginning and end of the frequency range where we look for lines.
    sub_range : tuple // corresponding subband numbers.
    Return :
    dict // containing sliced lines and corresponding : frequency and velocity ranges, rms value.
    name of the dict : CASA + date.
    """

    ### EXTRACTING PHASE
    print("Beginning : EXTRACTING phase")

    freq = np.arange(-200*df, 200*df, df)
    line = 'Calph'
    S    = []
    RMS  = []
    FREQ = []
    b_inf, b_sup = f_range ## in MHz
    k1, k2 = sub_range

    #### 1st method : Remove all points having more than 50% of weights<1 :


    for k in np.arange(k1, k2):
        DATA  = HDU.DATA[k*1024:(k+1)*1024]
        NDATA = HDU.NDATA[k*1024:(k+1)*1024]
        I = HDU.get_subband(HDU.I,k)
        f = HDU.get_subband(HDU.freq, k)
        subtable = NDATA.T
        ratio = []
        for i in range(1024):
            col = subtable[:,i]
            count = len(np.where(col < 1)[0])
            ratio.append(count/len(col))
        ratio = np.array(ratio)
        try :
            nI, chi, _ = Combined_flattening(HDU,k, fig = False)
        except :
            print('bad subband :', k)
            pass
        nI_copy = np.copy(nI)
    
        indexes = []
        for chan in range(1024) : #### removing points<0.5
            weight = ratio[chan]
            if weight > np.mean(ratio)+3*np.std(ratio) :
                indexes.append(chan)
    
        for chan in indexes : #### removing points<0.5
            nI_copy[chan] = np.nan
    
        ### masking central channels :
        for i in np.arange(508, 518):
            nI_copy[i] = np.nan
    
        LINES = HDU.select_lines(line,k)
    
    
    
        nI_masked, rms = mask(nI_copy, f, LINES, k)
    
        #### strict selection
        SLICE = slice_line(LINES, nI_copy,f)
        FREQS = slice_line(LINES, f, f)
        for i in range(len(LINES)-1,-1, -1) :
            val = LINES.iloc[i]
            if val <= b_inf or val >= b_sup:
                SLICE.pop(i)
                FREQS.pop(i)
    
        S    += SLICE
        FREQ += FREQS
        RMS  += [rms]*len(SLICE)
        
    ### end of global loop
    print("End : EXTRACTING phase")
    
    ### CLEANING PHASE
    print("Beginning : CLEANING phase")

    subset = []
    subvel = []
    subfre = []

    ## 2nd method : sigma clipping of residuals between model and line

    for i,s in enumerate(S) :
        WRONGS = [1]
    
        f  = FREQ[i]
        f0 = np.nanmean(f)
        v  = f_to_v(np.arange(-200*df, 200*df, df), f0)
               
        line = np.array(s)[~np.isnan(s)] ### test de la methode
        velocity = v.value[~np.isnan(s)]/1e3
    
        while len(WRONGS) > 0 :
            bounds = ([-2, -np.inf, 0, 0], [2, 0, np.inf, np.inf])
            popt, pcov = curve_fit(voigt, velocity, line, maxfev = 2000, bounds = bounds)
            residuals = line - voigt(velocity, *popt)
            new_res, indexes = recursive_clipping(residuals)
        
            for i in indexes :
                line[i] = np.nan
            velocity = velocity[~np.isnan(line)]
            line = line[~np.isnan(line)]
            WRONGS = indexes
        subset.append(line)
        subvel.append(velocity)
    ### end of cleaning loop
    
    print(len(subset), len(subvel), len(FREQ))
    
    print("End : CLEANING phase")
    
    
    ### FORMATTING
    print("Beginning : FORMATTING phase")
    
    new_S   = []
    new_vel = []
    for n in range(len(subset)) :
        l = subset[n]
        v = subvel[n]
        f  = FREQ[n]
        f0 = np.nanmean(f)
    
        l_ref = np.ones(400)
        v_ref = f_to_v(np.arange(-df*200,df*200,df), f0).value/1e3
        for i in range(400):
            if v_ref[i] in v :
                index = np.where(v == v_ref[i])[0]
                l_ref[i] = l[index]
            else :
                l_ref[i] = np.nan
        plt.plot(v_ref,l_ref)
        new_S.append(l_ref)
        new_vel.append(v_ref)
    ### end of reconstructing loop  
    
    print("End : FORMATTING phase")
    
    plt.show()
    
    ### NAMING AND SAVING
    
    dictionary = {'lines' : new_S, 'frequencies' : FREQ, 'velocities' : new_vel, 'RMS' : RMS}
    return dictionary


### comparison function


### fitting functions

### fonctions
def slice_line(LINES, I,f, velo, cut_width = 200): ## (I,f) = tau
    S = []
    for i,line in enumerate(LINES) :
    ### recuperation de la raie
        lenght = len(I)
        #f_raie  = line/(1+velo/(v_light).to(u.km/u.s).value)
        f_raie = line
        n_min = np.where(f < f_raie)[0]
        if len(n_min)>0 :
            n_r_min = n_min[-1]
        else :
            n_r_min = 0
        n_max = np.where(f > f_raie)[0]
        if len(n_max)>0 :
            n_r_max = n_max[0]
        else :
            n_r_max = lenght
        
        Sliced_left  = list(I[max(n_r_min - (cut_width-1),0):n_r_min])
        left  = len(Sliced_left) - cut_width
        if left < 0 :
            Sliced_left  = [np.nan for l in range(np.abs(left))] + Sliced_left
        
        Sliced_right = list(I[n_r_max:min(n_r_max + cut_width, lenght)])
        right = len(Sliced_right) - cut_width
        if right < 0 :
            Sliced_right = Sliced_right + [np.nan for l in range(np.abs(right))]
            
        if len(Sliced_right)+len(Sliced_left) != 2 * cut_width :
            print(f'subband = {k}',f'line = {i}', len(Sliced_right), len(Sliced_left))
        Sliced_line = Sliced_left + Sliced_right
        S.append(Sliced_line)
        #print(f"frequency of line : {f_raie} MHz")
        
    return S


def f_to_v(f, f0):
    """
    Converts frequency to velocity, using : $\frac{v}{c} = - \frac{f}{f_0}$.

    Parameters
    ----------
    :param f: frequency to convert.
    :type f: float.
    :param f0: reference frequency.
    :type f0: float.
    :return: velocity.
    :rtype: value*m/s.
    """
    c = 3e8 * u.m/u.s
    fr = f * u.MHz
    f0 = f0 * u.MHz
    return -c*fr/f0


def voigt(v, x_0,a_L, fwhm_L, fwhm_G): # model function
    func_voigt = Voigt1D(x_0 = x_0, amplitude_L = a_L, fwhm_L = fwhm_L, fwhm_G = fwhm_G)
    return func_voigt(v)

def Wrongs(I_masked, rms, display = False):
    n = len(I_masked)
    wrongs = []
    for i in range(n) : ## parcours de tous les points
        if np.abs(I_masked[i] - np.nanmean(I_masked)) > rms :
            wrongs.append(i)
    if display == True :
        fs = [f[k] for k in wrongs]
        Is = [I[k] for k in wrongs]
        fig = plt.figure(figsize = (11,3))
        gs = plt.GridSpec(1,1)
        ax = fig.add_subplot(gs[0])
        ax.plot(f, I_masked,'.')
        ax.set_ylabel('Tau')
        ax.set_xlabel('Frequency [MHz]')
        ax.plot(fs, Is, 'o', label = 'rfi')
        ax.legend()
        plt.show()
    return wrongs

def recursive_clipping(signal, threshold):
    """
    Turns all points of a signal that are above a given threshold to nan value.


    Parameters
    ----------
    :param signal: signal to clean.
    :type signal: array.
    :param threshold: absolute threshold outside which points will be removed.
    :type threshold: float.
    :return signal_clean: cleaned signal (same lenght as :param: `signal`), removed points.
    :rtype: signal_clean: array, list.
    """
    signal_clean = np.copy(signal)
    sigma = threshold
    wrongs = Wrongs(signal_clean, sigma)
    for w in wrongs :
        signal_clean[w] = np.nan
    return signal_clean, wrongs


def multiple_voigt(v, centers, amplitudes, lorentz_widths, gaussian_widths) :
    """

    Parameters
    ----------
    :param v  : x axis.
    :param centers  : centers of each voigt profiles (len = nb).
    :param amplitudes  : lorentz amplitudes of each voigt profiles (len = nb).
    :param lorentz_widths  : lorentz width of each voigt profiles (len = nb).
    :param gaussian_widths : gaussian width of each voigt profiles (len = nb).
    """
    Voigt = 0
    nb = len(centers)
    for i in range(nb) :
        """
        x_0 : center of the voigt profile.
        a_L : amplitude of lorentzian.
        fwhm_L : fwhm of lorentzian.
        fwhm_G : fwhm of gaussian.
        """
        
        x_0 = centers[i]
        a_L = amplitudes[i]
        fwhm_L = lorentz_widths[i]
        fwhm_G = gaussian_widths[i]
        
        voigt_profile = voigt(v, x_0, a_L, fwhm_L, fwhm_G)
        
        Voigt += voigt_profile
    
    return Voigt
    

def v_to_f(v,f0):
    """
    see f_to_v.
    """
    c = 3e8 * u.m/u.s
    vv = v * u.km/u.s
    f0 = f0 * u.MHz
    return -vv.to(u.m/u.s)/c*f0

def voigt_area(amp, gamma, sigma):
    """
    Area under the Voigt profile using the expression provided by Sorochenko & Smirnov (1990).

    Parameters
    ----------
    gamma : :obj:`float`
          Full width at half maximum of the Lorentzian profile.
    sigma : :obj:`float`.
          Full width at half maximum of the Doppler profile.
    """
    fwhm = voigt_fwhm(sigma, gamma)
    
    l = 0.5*gamma
    g = np.sqrt(2*np.log(2))*sigma
    k = g/(g+l)
    c = 1.572 + 0.05288*k + -1.323*k**2 + 0.7658*k**3
    
    return c*amp*fwhm

def voigt_fwhm(dD, dL):
    """
    Computes the FWHM of a Voigt profile. 
    http://en.wikipedia.org/wiki/Voigt_profile#The_width_of_the_Voigt_profile.
    
    .. math::
    
        FWHM_{\\rm{V}}=0.5346dL+\\sqrt{0.2166dL^{2}+dD^{2}}.
    

    Parameters
    ----------
    :param dD: FWHM of the Gaussian core.
    :type dD: array.
    :param dL: FWHM of the Lorentz wings.
    :type dL: array.
    :returns: The FWHM of a Voigt profile.
    :rtype: array.
    """
    
    return np.multiply(0.5346, dL) + np.sqrt(np.multiply(0.2166, np.power(dL, 2)) + np.power(dD, 2))

def a_Lorentz(area, gamma, sigma, fwhm):
    """
    
    Parameters
    ----------
    area : integrated intensity (km/s).
    fwhm : full width half max of voigt (km/s).
    gamma : fwhm of lorentzian contrib. (km/s).
    sigma : fwhm of doppler contrib. (gaussian) (km/s).
    """
    
    p = 1.57 - 0.507*np.exp(-0.85*gamma/sigma)
    
    return area / p / fwhm

def fwhm_G(fwhm, fwhm_L) :
    w = fwhm
    g = (0.5346 + np.sqrt(0.2166))*fwhm_L
    
    return np.sqrt(w - g)

### uncertainty functions

def Dsigma(sigma, Dw, Dg):
    Ds2 = 1/4/sigma**2 * Dw**2 + (0.5346 + np.sqrt(0.2166))**2/4/sigma**2 * Dg**2
    Ds  = np.sqrt(Ds2)
    return Ds

def Da_L(a, I, DI, w, Dw, g, Dg, s, Ds) :
    """
    a = \frac{I}{pw}.
    Da² = (da/dI)² * DI² + (da/dc)² * Dc² + (da/dw)² * Dw².
               DaDI2            DaDc2             DaDw2.
    """
    
    ## premier terme
    
    DaDI2 = (a/I)**2 * DI**2
    
    ## troisieme terme 
    
    DaDw2 = (-a/w)**2 * Dw**2
    
    ## deuxieme terme
    """
    da/dc = -a/c.
    c = 1.572 + 0.05288*k + -1.323*k**2 + 0.7658*k**3.
    k = sqrt(2*ln2)*s / (sqrt(2*ln2)s + 0.5g).
    
    Dc² = (dc/dk)² * Dk².
            dcdk.
    Dk² = (dk/ds)² * Ds² + (dk/dg)² * Dg².
            dkds             dkdg.
    """
    k = np.sqrt(2*np.log(2)) * s / (np.sqrt(2*np.log(2)) * s + 0.5 * g)
    c = 1.572 + 0.05288*k + -1.323*k**2 + 0.7658*k**3
    
    dkds = ( np.sqrt(2*np.log(2)) * (np.sqrt(2*np.log(2)) * s  + 0.5*g) - 2*np.log(2)*s ) / (np.sqrt(2*np.log(2)) * s + 0.5 * g)**2
    
    dkdg = (-np.sqrt(2*np.log(2)) * s * 0.5 ) / (np.sqrt(2*np.log(2)) * s + 0.5 * g)**2
    
    dcdk = 0.05288 - 2 * 1.323 * k + 3 * 0.7658 * k**2
    
    Dc2 = dcdk**2 * (dkds**2 * Ds**2 + dkdg**2 * Dg**2)
    DaDc2 = (-a/c)**2 * Dc2
    
    return np.sqrt(DaDI2 + DaDc2 + DaDw2)


def importing_with_correction(bloc, velo):
    """ Imports an :class:`L1_class` object.

    Parameters
    ----------
    :param bloc: complete path to the observation bloc.
    :type bloc: str.
    :param velo: relative velocity of the observed object.
    :type velo: float.
    """
    ### choix du bloc d'observation
    path_fits = bloc
    path_rrls = 'rrlines.csv'

    hdu = L1.L1(path_fits, path_rrls)
    hdu.get_date()
    hdu.integration()
    return hdu

def importing(bloc):
    """ Imports an :class:`L1_class` object, and computes its integrated intensity (relative).

    Parameters
    ----------
    :param bloc: complete path to the observation bloc.
    :type bloc: str.
    """
    ### choix du bloc d'observation
    path_fits = bloc
    path_rrls = 'rrlines.csv'

    hdu = L1.L1(path_fits, path_rrls)
    hdu.get_date()
    hdu.integration()
    return hdu

def fwhm_voigt_err(Df_L, Df_G, f_L, f_G):
    """
    Parameters
    ----------
    :param Df_L: error on lorentzian fwhm.
    :param Df_G: error on gaussian fwhm.
    :param f_L: lorentzian fwhm.
    :param f_G: lorentzian fwhm.
    """
    return 0.5346*Df_L + (0.2166*f_L*Df_L + f_G*Df_G)/np.sqrt(0.2166*f_L**2 + f_G**2)

def voigt_fwhm_LC(f_L,f_G):
    return(np.multiply(0.5346, f_L) + np.sqrt(np.multiply(0.2166, np.power(f_L, 2)) + np.power(f_G, 2)))

def voigt_fitting_Pedro(velocity, stack, velocities, f0, df):
    #df = 0.00019073486328125
    #df = 0.0003814
    nb = len(velocities)
    
    dv = f_to_v(df,f0).to(u.km/u.s).value
    print(dv)
    
    
    deltav = 50. # np.abs(velocity[-1] - velocity[0])/2.
    
    convo0 = 0.5*np.sqrt(2**2 + dv**2)
    convo1 = 0.5*np.sqrt(2**2 + dv**2)
    convo2 = 0.5*np.sqrt(2.5**2 + dv**2)
    
    
    if nb == 3 :
        shift_0,shift_1,shift_2 = velocities
        #voigt_fct = triple_voigt_PS
        voigt_fct = lambda v, x_0_47, a_L_47, fwhm_L_47, a_L_38, fwhm_L_38, a_L_50, fwhm_L_50 : triple_voigt_PS(v, x_0_47, a_L_47, fwhm_L_47, convo0,
                                                                                                                shift_1, a_L_38, fwhm_L_38, convo1,
                                                                                                                shift_2, a_L_50, fwhm_L_50, convo2)

        bounds = ([shift_0 - 5 , -np.inf,      0, -np.inf,      0, -np.inf,      0],
                  [shift_0 + 5 ,       0, deltav,       0, deltav,       0, deltav])
    #             x047,    aL47, fwhmL47,x038,    aL38, fwhmL38,x00,     aL0, fwhmL0
    
    if nb == 2 :
        shift_0,shift_1 = velocities
        #voigt_fct = double_voigt_PS
        
        voigt_fct = lambda v, x_0_47, a_L_47, fwhm_L_47, a_L_38, fwhm_L_38 : double_voigt_PS(v, x_0_47, a_L_47, fwhm_L_47, convo0,
                                                                                            shift_1, a_L_38, fwhm_L_38, convo1)

        
        bounds = ([shift_0 - 5 , -np.inf,       0, -np.inf,      0],
                  [shift_0 + 5 ,       0,  deltav,       0, deltav])
    #                      x047,    aL47,  fwhmL47,    aL38, fwhmL38
    
    if nb == 1 :
        shift_0 = velocities[0]
        #voigt_fct = simple_voigt_PS
        
        voigt_fct = lambda v, x_0_47, a_L_47, fwhm_L_47 : simple_voigt_PS(v, x_0_47, a_L_47, fwhm_L_47, convo0)

        bounds = ([shift_0 - 5 , -np.inf,       0],
                  [shift_0 + 5 ,       0,  deltav])
    #                      x047,    aL47, fwhmL47
    #print(voigt_fct)
    #return voigt_fct
    popt, pcov = curve_fit(voigt_fct, velocity, stack, bounds = bounds, maxfev = 3000)
    #popt, pcov = curve_fit(voigt_fct, velocity, stack)
    
    plt.step(velocity,stack)
    plt.plot(velocity, voigt_fct(velocity, *popt))
    #plt.show()
    #print("Voigt FItting PS")
    
    return popt, pcov, voigt_fct, (convo0, convo1, convo2)

def voigt_fitting_FINAL(v, stack, velo_shift, f0, df, turbu):
    """
    Fitting a voigt profile (gaussian + lorentzian).

    Parameters
    ----------
    :param v: x axis (km/s).
    :type v: array.
    :param stack : y axis (tau).
    :type stack: array.
    :param velo_ : velocity components (delta from center) (len = nb).
    :type velo_: list of floats.
    :param f0 : absolute frequency of line (MHz).
    :type f0: float.
    :param df : spectral resolution (MHz).
    :type df: float.
    :param turbu : turbulence factor of the cloud (len = nb).
    :type turbu: list of floats.
    :returns: optimal parameters of the fit, covariance matrix of the fit, function that was fitted, gaussian widths of the voigt profiles.
    """

    nb = len(velo_shift)
    
    ## velocity resolution of data (km/s)
    dv = f_to_v(df,f0).to(u.km/u.s).value
    
    ## half lenght of line (km/s)
    deltav =  np.abs(v[-1] - v[0])/2.
    
    
    ## convolution of expected data to velocity resolution
    
    convos = [] 
    for i in range(nb):
        convo = 0.5*np.sqrt(turbu[i]**2 + dv**2)
        convos.append(convo)

    str_command = 'lambda v, center, '
    ampli = '[ a_0, '
    str_command+= 'a_0,'
    for i in range(1,nb):
        str_command+= f' frac_{i}, '
        ampli+= f'frac_{i}*a_0,'
    ampli = ampli[:-1] + ']'
    lws = '['
    for i in range(nb):
        str_command+= f'lw_{i}, '
        lws+= f'lw_{i},'
    lws = lws[:-1] + ']'
    cent = '['
    for i in range(nb):
        cent += f'center + {velo_shift[i]},'
    cent = cent[:-1] + ']'
    str_command = str_command[:-2] + f' : multiple_voigt(v, {cent}, {ampli}, {lws}, {convos})'
    
    voigt_fct = eval(str_command)
    bounds    = ([velo_shift[0] - 5] + [-1]*nb +      [0]*nb,
                 [velo_shift[0] + 5] +  [0]*nb + [deltav]*nb)
    
    
    popt, pcov = curve_fit(voigt_fct, v, stack, bounds = bounds, maxfev = 3000)
    
    
    #plt.step(velocity, stack)
    #plt.plot(velocity, voigt_fct(v, *popt))
    
    return popt, pcov, voigt_fct, convos


def voigt_area_LC(peak, f_L, f_G):
    """
    Area under the Voigt profile using the expression provided by Sorochenko & Smirnov (1990).
    
    Parameters
    ----------
    peak : :obj:`float`.
          Peak of the Voigt line profile.
    fwhm : :obj:`float`.
          Full width at half maximum of the Voigt profile.
    gamma : :obj:`float`.
          Full width at half maximum of the Lorentzian profile.
    sigma : :obj:`float`.
          Full width at half maximum of the Doppler profile.
    """
    fwhm = voigt_fwhm(f_L, f_G)
    p = 1.57 - 0.507*np.exp(-0.85*f_L/f_G)
    
    return peak*fwhm*p

fwhm_fct = lambda lw, gw : np.multiply(0.5346, lw) + np.sqrt(np.multiply(0.2166, np.power(lw, 2)) + np.power(gw, 2))
