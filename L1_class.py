# -*- coding: utf-8 -*-

"""
Created on Wed Jun  9 09:37:41 2021

@author: lucie
"""

from astropy.io import fits
from matplotlib import pylab as plt
import numpy as np
import calcul
import matplotlib.cm as CM
from astropy.time import Time


### classe : L1

class L1 :
    def __init__(self, file, rrlfile):
        hdu = fits.open(file)
        self.hdu = hdu
        ### inputs
        data = hdu[1].data
        names = data.columns
        self.rrlfile = rrlfile
        
        for el in names :   
            na = el.name
            param = data[el.name]  
            ### initialization de l'attribut
            #exec('self.' + na + ' = ' + str(param))
            setattr(self,na,param)
              
        
        ### outputs
        data = hdu[2].data
        names = data.columns
        for el in names :
            na = el.name
            param = data[el.name]  
            setattr(self,na,param)
        
        ### tables
        
        for i in range(3,len(self.hdu)) :
            key = hdu[i].header['ARRAY']
            na = key
            param = hdu[i].data 
            setattr(self,na,param)
        
        ### command line
            
        Prim = hdu[0].data
        Byt = bytearray(Prim)
        ST = Byt.decode()
        self.command_line = ST
        
        ### display parameters
        self.names_cols = {'Halph'  : 'darkblue',
              'Hbeta'  : 'mediumblue',
              'Hgamm'  : 'b',
              'Hdelt'  : 'blue',
              'Hepsi'  : 'royalblue',
              'Healph' : 'orange',
              'Hebeta' : 'gold',
              'Calph'  : 'darkviolet',
              'Cbeta'  : 'violet',
              'Salph'  : 'yellowgreen'}
    
    def get_date(self):
        s = self.command_line
        original_file = s.split(',')[1].replace("'","")
        path = original_file.split('/')
        name = path[-1].split('_')
        date = name[3]
        hour = name[4]
        total_time = date[:4] + '-' + date[4:6] + '-' + date[6:] + 'T' + hour[:2] + ':' + hour[2:4] + ':' + hour[4:]
        self.date = Time(total_time)    
        year = self.date.value.split('T')[0]
        time = self.date.value.split('T')[1].split('.')[0]
        self.year = year
        self.time = time
        return self.date, self.year, self.time

        
    def filter_weights(self):
        NDATA = self.NDATA
        MASK = np.ma.masked_less(NDATA,1).mask
        if self.NS[0] > 1 :
            DATA = self.DATA[0]
        else :
            DATA = self.DATA
        m_DATA = np.ma.masked_array(DATA, mask = MASK, fill_value = np.nan).filled()     
        m_I = np.nanmedian(m_DATA, axis = 1)
        
        return m_I
    
    
    def integration(self, rebin = False, interval = 1, weight = False):
        NDATA = self.NDATA
        if self.NS[0]>1 :
            DATA = self.DATA[0]
            sum_cols = DATA.shape[1]
        else :
            DATA = self.DATA
            sum_cols = np.sum(NDATA,axis = 1)
        I =  np.sum(DATA, axis = 1)/sum_cols
        if rebin == False and weight == False :            
            mask = np.ma.masked_equal(I,0)
            self.I = np.ma.masked_array(I, mask = mask.mask, fill_value = np.nan).filled()
            self.freq = self.FREQ
        elif weight == True :
            I = self.filter_weights()
            if rebin == True :
                self.I, self.freq = calcul.running_avg(I,self.FREQ, width = interval)
            else :
                self.I = I
                self.freq = self.FREQ
        else :
            rebin_I, self.freq = calcul.running_avg(I,self.FREQ, width = interval)
            mask = np.ma.masked_equal(rebin_I,0)
            self.I = np.ma.masked_array(rebin_I, mask = mask.mask, fill_value = np.nan).filled()
        
    def display(self, k1, k2):
        k2=k2+1
        I = self.I
        nsub = self.NBEAMLETS[0]
        n = int(len(I)/nsub)
        band = I[n*k1:n*k2]
        bandf = self.freq[n*k1:n*k2]
        plt.figure(figsize=(10,2.5))
        plt.plot(bandf, band)
        plt.title(f'Integrated intensity \n - observation date {self.date} - \n - between {k1} and {k2-1}th subband -')
        plt.ylabel('Stokes I [relative]')
        plt.xlabel('Frequency [MHz]')
        
        plt.show()

    def display_range_PS(self, start, nband):       
        k1=start
        k2=start+nband
        I = self.I
        nsub = self.NBEAMLETS[0]
        n = int(len(I)/nsub)
        band = I[n*k1:n*k2]
        bandf = self.freq[n*k1:n*k2]
        plt.figure(figsize=(10,4.5))
        plt.plot(bandf, band)
        plt.title(f'Integrated intensity \n - observation date {self.date} - \n - between {k1} and {k2-1}th subband -')
        plt.ylabel('Stokes I [relative]')
        plt.xlabel('Frequency [MHz]')
           
        
    def import_rrls_PS(self, v = -47, correction = True): ## velocity in km/s
        import pandas as pd
        import astropy.units as u
        fmin = self.FMIN[0]
        fmax = self.FMAX[0]
        RRL = pd.read_csv(self.rrlfile, sep = ';')
        RRL.set_index('n', inplace = True)
        working_RRL = RRL 
        #w_RRL_max = RRL.where(working_RRL < fmax)
        #w_RRL = w_RRL_max.where(w_RRL_max > fmin)
        cols = working_RRL.columns
        A = dict()
        self.get_date()
        if correction == True :
            V = - calcul.doppler_corrections(self.date)[-1].to(u.km/u.s).value + v
            #V = calcul.corrected_velocities(v, self.date)*1000
        else :
            V = v*1000
        for c in cols :
            #serie = w_RRL[c]
            serie = working_RRL[c]
            A[c] = serie.dropna()
        #    
        #    ### correcting 
        #
        #    print(V)
            A[c] = calcul.doppler_correction(A[c], V)
        
        #self.RRLS = RRL
        self.RRLS_doppler = A
        self.RRLS = RRL
        return A


    def display_lines_band(self, k, velo, myLine = 'Calph'):
        expected_comp = self.import_rrls_PS(v=velo, correction = False)
        nsub = self.NBEAMLETS[0]
        n = int(len(self.freq)/nsub)       
        fmin, fmax = self.freq[n*k], self.freq[(k + 1)*n-1]

        lines=[]

        keys = [myLine]
        for key in keys:
            smin=expected_comp[key].where(expected_comp[key] > fmin)
            sminmax = smin.where(smin < fmax)
            sfin=np.array(sminmax.dropna())
            #print(sfin)
            #for i in range(len(sfin)):
            #    plt.vlines(sfin[i], np.nanmin(self.get_subband(self.I,k)), np.nanmax(self.get_subband(self.I,k)), color=self.names_cols[key])
            lines.append(sfin)
        return np.concatenate(np.array(lines), axis=0 )

    
    
    def select_lines(self, line_name, subband_nb):
        try : 
            serie = self.RRLS_doppler[line_name]
        except :
            serie = self.import_rrls_PS()
            #serie = self.RRLS_doppler[line_name]
        nsub = self.NBEAMLETS[0]
        n = int(len(self.freq)/nsub)       
        fmin, fmax = self.freq[n*subband_nb], self.freq[(subband_nb + 1)*n-1]
        smin = serie.where(serie > fmin)
        sminmax = smin.where(smin < fmax)
        sfin = sminmax.dropna()
        return sfin
    
    
    
    def get_subband(self, array, k):
        nsub = self.NBEAMLETS
        n = int(len(self.freq)/nsub)
        return array[k*n : (k+1)*n]
    
    def get_tau(self, k, rebin_interval, line_name, lim):
        freq = self.FREQ
        try :
            I = self.I
        except :
            self.integration()
            I = self.I
        
        subf = self.get_subband(freq, k)
        subI = self.get_subband(I, k)
        RRL_selected = np.array([])
        for line in line_name :
            RRL_selected = np.concatenate((RRL_selected,self.select_lines(line, k).values), axis = 0)
        
        TAU = calcul.flatten(subf, subI, rebin_interval, RRL_selected, lim = lim)
        
        self.TAU = TAU
        
        ### display
        

    def display_with_lines(self, line_name, subband_nb, tau = True): ### line_name = array like
        self.get_date()
        freq = self.get_subband(self.FREQ, subband_nb)
        if tau == True :
            I,rebin,lim,_ = self.post_flat_mitigation(line_name, subband_nb)
            #self.get_tau(subband_nb, 20, line_name, 60)
            
            #I = self.TAU
            freq = freq[lim:-lim]
        else :
            I = self.get_subband(self.I, subband_nb)
        #N = self.NF
        fig = plt.figure()
        gs = plt.GridSpec(1, 1)
        ax = fig.add_subplot(gs[0])
        ax.plot(freq,I, '.')
        ax.set_title(f'Integrated intensity \n - observation date {self.date} - \n - {subband_nb}th subband -')
        ax.set_ylabel('Tau')
        ax.set_xlabel('Frequency [MHz]')
        for line in line_name :
            RRL_selected = self.select_lines(line, subband_nb)
            inds = RRL_selected.index.values

            for i,l in enumerate(RRL_selected): 
                IND = inds[i]
                n = IND # + 40
                ax.vlines(l , np.nanmin(I), np.nanmax(I), self.names_cols[line],label = line + f' : n = {n}') 

                #plt.text(l,np.nanmax(I),line + f', n = {n}')
        #plt.legend()
        #fig.tight_layout()
        return fig
        
    def get_freq(self,k):
        n = self.NCHANNELS[0]
        df = self.DF[0]/1000
        fmin = self.FMIN[0]
        fr = fmin + k*n*df
        return fr
    
    def ratio_flagged(self, critere, subband = True): ## critere = string de commande
        fig = plt.figure(**{'figsize' : (10,5)})
        NDATA = self.NDATA
        n = int(self.NCHANNELS[0])
        dt = int(self.NT[0])
        nsub = self.NBEAMLETS[0]
        df = int(self.NF[0])
        if subband == True :
            LIST = []
            for i in range(nsub) :
                
                subband = NDATA[i*n:(i+1)*n,:]
                zero = len(eval('np.where(subband ' + critere + ')[0]'))
                pixel = n*dt
                LIST.append(zero/pixel)

                K = np.array(range(nsub))
                freqs = self.get_freq(K)
                plt.suptitle('Percentage of flagged data for each subband \n flag : weight ' + critere)

        else :
            LIST = []
            for i in range(df):
                col = NDATA[i,:]
                zero = len(eval('np.where(col ' + critere + ')[0]'))
                LIST.append(zero/len(col))
                freqs = self.FREQ
                plt.suptitle('Percentage of flagged data for each frequency \n flag : weight ' + critere)


        
        ax1 = fig.add_subplot(111)
        
    

        ax1.plot(freqs,LIST, '.')
        ax1.set_ylim([0,1.05])
        ax1.set_xlim([int(self.FMIN[0]), int(self.FMAX[0])+1])
        ax1.set_ylabel('Fraction of data flagged')
        ax1.set_xlabel('frequencies')
        
        return LIST
    
    def display_flags_per_subband(self,k):
        sub = self.get_subband(self.I, k)
        subf = self.get_subband(self.freq,k)
        NDATA = self.NDATA
        n = int(self.NCHANNELS[0])
        subtable = NDATA[k*n:(k+1)*n,:].T
        
        ### binary sort
        #MASK = np.array(np.ma.masked_equal(subtable, 1).mask, dtype = float)
        
        ratio = []
        for i in range(int(self.NCHANNELS[0])):
            col = subtable[:,i]
            count = len(np.where(col < 1)[0])
            ratio.append(count/len(col))
        ratio = np.array(ratio)*100
        
        ### infos
        self.get_date()
        
        
        fig = plt.figure(**{"figsize" : (10,8)})
        gs = plt.GridSpec(2, 2, width_ratios= (40,1))
        ax0 = fig.add_subplot(gs[0])
        ax1 = fig.add_subplot(gs[1])
        plt.suptitle(f'RFI flagging on the {k}-th subband, observation date : {self.date}')
        
        mapp = ax0.imshow(np.array(subtable, dtype = float), aspect = 'auto', cmap = 'gist_gray')
        ax0.imshow(np.array(subtable, dtype = float), aspect = 'auto', cmap = 'gist_gray')
        ax0.set_title('Statistical weights of each pixel of the subband')
        ax0.set_xticks([])
        ax0.set_ylabel('Temporal resolution')
        fig.colorbar(mappable = mapp, cax = ax1)
        ax2 = fig.add_subplot(gs[2])
        ax2.scatter(subf,sub, s = 1.5,c = ratio, cmap = 'viridis')
        ax2.set_xlabel('Frequency [MHz]')
        
        xtick = ax2.get_xticks()
        new_xtick = [f"{i:.2f}" for i in xtick]
        ax2.set_xticklabels(new_xtick, rotation = 45)
        
        ax2.set_title('Integrated intensity')
        ax3 = fig.add_subplot(gs[3])
        cbar2 = fig.colorbar(mappable = CM.ScalarMappable(cmap = 'viridis'), cax = ax3)
        cbar2.set_label('percentage of weights < 1')
        #cbar2.ax.set_yticks( vmin = 0, vmax = 100)
        plt.tight_layout()
        plt.show()
        
        
    def display_all_lines(self, k):
        rrls = self.RRLS    
        for key in rrls.keys():
            self.display_with_lines(key, k, False)
    
    def get_tau_linear(self,k, lim, figu = False):
        sub = self.get_subband(self.I, k)
        subf = self.get_subband(self.freq, k)
        sub_lim = sub[lim:-lim]
        useful = np.where(sub_lim != np.nan)[0]
        subf_lim = subf[lim:-lim]
        
        sub_lim  = sub_lim[~np.isnan(sub_lim)]
        subf_lim = subf_lim[~np.isnan(subf_lim)]
        
        
        pair0 = [subf_lim[0], subf_lim[-1]]
        pair1 = [sub_lim[0],  sub_lim[-1]]
        print(pair0,pair1, k)
        from scipy.interpolate import CubicSpline as cs
        F_interpolate = cs(pair0,pair1)
        self.TAU = sub[lim:-lim]/F_interpolate(subf[lim:-lim])-1

    def rms(self,Sk):
        N = len(Sk[~np.isnan(Sk)])
        return np.sqrt(np.nansum(Sk**2))/N
    
    def rms_per_subband(self):
        n_beam = self.NBEAMLETS[0]
        RMS = np.zeros(n_beam)
        lines = ['Halph','Hbeta','Calph','Cbeta']
        for i in range(n_beam):
            try :
                mitig,lim, rebin, rms = self.post_flat_mitigation(lines, i)
                RMS[i] = rms
            except :
                RMS[i] = np.nan
        K = np.arange(n_beam)
        freq = self.get_freq(K)
        fig = plt.figure()
        gs = plt.GridSpec(1, 1)
        ax = fig.add_subplot(gs[0])
        ax.plot(freq, np.log(RMS)/np.log(10), '.')
        ax.set_xlabel('Frequency [MHz]')
        ax.set_ylabel('log(RMS)')
        ax.set_title(f'Root mean square per subband, obs = {self.date}')
        plt.tight_layout()
        return RMS
    
    def optim(self, lines,subnb, figu = False):
        lims = []
        for i in range(60,120):
            self.get_tau(subnb,20,lines,i)
            lims.append(self.rms(self.TAU))
        minlim = np.argmin(lims) + 60
        rebin = []
        for k in range(10,30):
            self.get_tau(subnb, k, lines, minlim)
            rebin.append(self.rms(self.TAU))
        minrebin = np.argmin(rebin) + 10
        if figu == True :
            self.get_tau(subnb, 20, lines, 60)
            freq = self.get_subband(self.freq, subnb)[60:-60]
            rms = self.rms(self.TAU)
            plt.plot(freq,self.TAU, label = f'Generic params : {rms : .2e}')

            self.get_tau(subnb,minrebin, lines, minlim)
            rms = self.rms(self.TAU)
            freq = self.get_subband(self.freq, subnb)[minlim:-minlim]
            plt.plot(freq,self.TAU, label = f'Optimized : {rms : .2e}')
            #plt.title(f'Optimal rms : {np.min(rebin)}')
            plt.legend()
            plt.show()
        return minrebin, minlim, np.min(rebin)   
    
    def post_flat_mitigation(self,lines, subnb, figu = False):
        sub = self.get_subband(self.I,subnb)
        rebin, lim, rms = self.optim(lines, subnb)
        self.get_tau(subnb,rebin,lines,lim)
        TAU = self.TAU
        freq = self.get_subband(self.freq, subnb)[lim:-lim]
        

        std = np.nanstd(TAU)
        mask = np.ma.masked_where(TAU>3*std, TAU).mask
        new_arr = np.ma.masked_array(TAU, mask = mask, fill_value = np.nan).filled()
        new_rms = self.rms(new_arr)
        if figu == True :
            fig = plt.figure()
            fig.suptitle(f'Post-flat RFI mitigation - subband nb {subnb} \n {self.year} {self.time}')
            gs = plt.GridSpec(2, 1)
            ax0 = fig.add_subplot(gs[0])
            ax0.plot(freq,TAU)
            ax0.set_ylabel('Tau')
            ax0.set_title('Flattened - no post mitigation')
            ax1 = fig.add_subplot(gs[1], sharey = ax0)
            ax1.plot(freq, new_arr)
            ax1.set_ylabel('Tau')
            ax1.set_title('Flattened - with post mitigation')
            ax1.set_xlabel('Frequency [MHz]')
            plt.tight_layout()
        return new_arr, rebin, lim, new_rms
        