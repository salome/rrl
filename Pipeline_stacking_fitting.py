#!/usr/bin/env python
# coding: utf-8


from calcul import *
from astropy.io import fits
import sys
import pickle
from astropy.modeling.models import Voigt1D , Gaussian1D
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
import time
import pandas as pd

"""
Pipeline for Stacking and fitting
"""

inPanda=True

if (inPanda==True):
    df=pd.read_csv('/obs/salome/ES10/es10-share/rrl/init.csv', ' ')
    print(df.columns)

    ## GET INFOS FROM USER                                                           
    start = time.time()
    # about the .fits                                                               
    parent = df['parent'][0]
    print(f'parent_path : {parent}')
    data = df['data'][0]
    print(f'data_path : {data}')
    Lane = df['Lane'][0]
    print(f'Lane : {Lane}')
    
    # physical parameters of the ISM                                             
    
    velo_1     = df['velo_1'][0]
    print(f'velo_1 : {velo_1}')
    velocities = df['velocities'][0]
    print(f'velocities : {velocities}')
    turbu      = df['turbu'][0]
    print(f'turbulence : {turbu}')
    myLine     = df['myLine'][0]
    print(f'my line : {myLine}')
    
    ############ formatting                                                      
    
    velocities=velocities[1:]
    velocities=velocities[:-1]
    velocities=velocities.split(",")
    velocities=np.array(velocities, dtype=float)

    
    turbu=turbu[1:]
    turbu=turbu[:-1]
    turbu=turbu.split(",")
    turbu=np.array(turbu, dtype=float)



    velos_ = dict()
    for i in range(len(velocities)):
        exec(f'velos_["velo_{i}"] = {velocities[i]} + velo_1')
        
    # data reduction parameters                                                      
                
    Nsigma = df['Nsigma'][0] # Clipping threshold for RFI : N x sigma
    print(f'Nsigma : {Nsigma}')
    width_of_line = df['width_of_line'][0] # width of the window of a line  
    print(f'width of line : {width_of_line}')

    # continuum parameters                                                           
    
    alpha = df['alpha'][0]
    print(f'alpha : {alpha}')
    contAtFreq = df['contAtFreq'][0]
    print(f'continuum at ref freq : {contAtFreq}')
    refFreqCont = df['refFreqCont'][0]
    print(f'reference freq : {refFreqCont}')
    # which data ?                                                                   
    
    band_inf = df['band_inf'][0]
    band_sup = df['band_sup'][0]
    print(f'between subband {band_inf} and {band_sup}')
    

modeSys=False

if (modeSys==True):
    ## GET INFOS FROM USER
    start = time.time()
    # about the .fits
    parent = str(sys.argv[1])
    print(f'parent_path : {parent}')
    data   = str(sys.argv[2])
    print(f'data_path : {data}') 
    Lane   = int(sys.argv[3])
    print(f'Lane : {Lane}')  

    # physical parameters of the ISM

    velo_1     = float(sys.argv[4])
    print(f'velo_1 : {velo_1}') 
    velocities = eval(sys.argv[5])
    print(f'velocities : {velocities}') 
    turbu      = eval(sys.argv[6])
    print(f'turbulence : {turbu}')
    myLine     = str(sys.argv[7])
    print(f'my line : {myLine}')

    ############ formatting
    velos_ = dict()
    for i in range(len(velocities)) :
        exec(f'velos_["velo_{i}"] = {velocities[i]} + velo_1')
    
        # data reduction parameters

        Nsigma        = int(sys.argv[8]) # Clipping threshold for RFI : N x sigma 
        print(f'Nsigma : {Nsigma}')
        width_of_line = int(sys.argv[9]) # width of the window of a line
        print(f'width of line : {width_of_line}')
        
        # continuum parameters
        
        alpha = float(sys.argv[10])
        print(f'alpha : {alpha}')
        contAtFreq = float(sys.argv[11])
        print(f'continuum at ref freq : {contAtFreq}')
        refFreqCont = float(sys.argv[12])
        print(f'reference freq : {refFreqCont}')
        # which data ?
        
        band_inf = int(sys.argv[13])
        band_sup = int(sys.argv[14])
        print(f'between subband {band_inf} and {band_sup}')

### Pre-loop data extraction

bloc = parent + data
HDU = importing(bloc)
df_ = eval(f"{HDU.DF[0]}*u.{HDU.DFUNIT[0]}")
df=df_.to(u.MHz).value
nb = len(velos_)

# lsr correction (constant) :

DELTAV = doppler_corrections(HDU.date)[-1].to(u.km/u.s).value
print("### velocity components of ISM - lsr corrected ### \n")
for i in range(len(velos_)):
    print(f"velo_{i} = {velos_[f'velo_{i}'] - DELTAV} km/s \n")

# Print some information about the data

print(f'date and time of observation : {HDU.get_date()}')
print(f'f_min = {HDU.FMIN[0]} MHz')
print(f'f_max = {HDU.FMAX[0]} MHz')
print(f'spectral resolution : {df} MHz')

# loop on each subband

for band in range(band_inf, band_sup + 1) :

######################################### FIRST STEP
	print(f'FIRST STEP for subband n°{band}')
	# Get the data (intensities no unit and frequencies)

	data_in_band = HDU.get_subband(HDU.I,band)
	freq_in_band = HDU.get_subband(HDU.freq, band)

	# expected lines, corrected
	lines = []
	for i in range(len(velos_)) :
    		lines += list(HDU.display_lines_band(band,velos_[f'velo_{i}'] - DELTAV, myLine))

	# Overplot the line free channels (with a mask of #chan=20 over each side of the line)

	I_masked, rms, mask = mask_fct(data_in_band,freq_in_band, lines, display = False)
	for i in range(HDU.NCHANNELS[0]//2 - 10, HDU.NCHANNELS[0]//2 + 10):
		I_masked[i] = np.nan
	II = I_masked[~np.isnan(I_masked)]
	ff = freq_in_band[~np.isnan(I_masked)]

	# flattening process (with savgol filter)
	x = freq_in_band[1:]
	y = I_masked[1:]
	try :
		y_interp = interp1d(ff,II)(x)
	except :
		print('error on interpolation')
		continue
	ysmooth = savgol_filter(y_interp,101,3,mode='interp',)
	
	# optical depth
	tau = data_in_band[1:]/ysmooth - 1
	tau = np.concatenate(([0], tau))
	
	# Clean Tau from the remaining RFI
	
	clipping_threshold=Nsigma*np.nanstd(tau)
	line_free_tau = np.ma.masked_array(tau,mask=mask).filled(np.nan)
	clean_line_free_tau_tmp, indexes = recursive_clipping(line_free_tau, clipping_threshold)

	new_tau, new_freq= running_avg(clean_line_free_tau_tmp, freq_in_band, 500, display = False)
	clipping_threshold=Nsigma*np.nanstd(new_tau)
	
	for i in indexes :
		tau[i] = np.nan
	
	clean_line_free_tau, indexes = recursive_clipping(clean_line_free_tau_tmp, clipping_threshold)
	clean_mean=np.nanmean(clean_line_free_tau_tmp)

	for i in indexes :
		tau[i] = np.nan

	# Compute the rms of Tau

	rms_flat_all = np.nanstd(clean_line_free_tau)

	print(f'RMS of line free optical depth = {rms_flat_all : .3e}')

	# Plot Tau (center+edges) without RFI and overplot expected lines

	plt.figure()
	plt.xlabel("Frequency (MHz)")
	plt.ylabel("Tau")

	for i in range(len(lines)):
		plt.vlines(lines[i], np.nanmin(tau), np.nanmax(tau), 'red')

	plt.plot(freq_in_band, tau)
	plt.plot(freq_in_band, clean_line_free_tau, ".")

	plt.plot(new_freq, new_tau, "o")
	plt.hlines(clean_mean+clipping_threshold, np.min(new_freq),np.max(new_freq))
	plt.hlines(clean_mean-clipping_threshold, np.min(new_freq),np.max(new_freq))

	plt.title("Tau for Band "+str(band)+", rms= "+str(rms_flat_all))
	plt.savefig(parent + f"{data.split('/')[0]}/PNG/tau-band-{band}-lane-{Lane}-line-{myLine}.png")
	
	
	# Convert Tau into Jy from a theoritical absolute flux spectral density of CAS A
	
	continuum = (1+clean_line_free_tau)*contAtFreq*(freq_in_band/refFreqCont)**-alpha
	cont = contAtFreq*(freq_in_band/refFreqCont)**-alpha
	signal=(1+tau)*contAtFreq*(freq_in_band/refFreqCont)**-alpha

	rms_flat=np.nanmean(cont*rms_flat_all)
	rms_signal = cont*rms_flat_all
	
	print(f'RMS on line free continuum : {rms_flat.mean()} Jy \n')
	
	## plotting continuum
	
	plt.figure(figsize=(10,4.5))
	plt.title("Signal for Band "+str(band)+" rms= "+str(rms_flat.mean())+" Jy")
	plt.plot(freq_in_band, continuum,'.')
	plt.plot(freq_in_band, cont)
	plt.plot(freq_in_band, signal)

	for i in range(len(lines)):
		plt.vlines(lines[i], np.nanmin(continuum), np.nanmax(continuum), 'red')


	plt.savefig(parent + f"{data.split('/')[0]}/PNG/signal-band-{band}-lane-{Lane}-line-{myLine}.png")

	
	d0 = {"freq": freq_in_band, "original": data_in_band, "tau": tau, "signal": signal, "rms_signal": rms_signal, "clean_line_free_tau":clean_line_free_tau, "df" : df, "turbu" : turbu, "velocities" : velocities, "velo_1" : velo_1, 'width_of_line' : width_of_line, 'myLine' : myLine}

	with open(parent + f"{data.split('/')[0]}/pickles/Band-"+str(band)+f"-output-lane-{Lane}-line-{myLine}.pickle", "wb") as output_file:
		pickle.dump(d0, output_file)
	
######################################### SECOND STEP
	print(f'SECOND STEP for subband n°{band}')
	## general info
	freq_band=HDU.get_freq(band) + df*HDU.NCHANNELS[0]//2
	df_ = eval(f"{HDU.DF[0]}*u.{HDU.DFUNIT[0]}")
	df=df_.to(u.MHz).value
	
	dict_ = d0

	# Cut the band at -width_of_line/2 and +width_of_line/2 around the line and display

	I = dict_['tau']
	f = dict_['freq']

	### centered on main velocity component 
	LINES=list(HDU.display_lines_band(band,velo_1 - DELTAV))


	Lines = slice_line(LINES, I, f, velo_1 - DELTAV, width_of_line//2)
	freqs = slice_line(LINES, f, f, velo_1 - DELTAV, width_of_line//2)


	# Cleaning the remaining RFI 
	# For each slice : voigt fitting, check the residuals and 
	# clip the residual at 2 sigma to remove RFI in the line window

	print("Beginning : CLEANING PHASE")
	subset = []
	subvel = []

	for i,s in enumerate(Lines) :
		WRONGS = [1]

		f  = freqs[i]
		f0 = np.nanmean(f)
		v  = f_to_v(np.arange(-width_of_line//2*df, width_of_line//2*df, df), f0)
               
		line = np.array(s)[~np.isnan(s)] ### test de la methode
		velocity = v.value[~np.isnan(s)]/1e3

		while len(WRONGS) > 0 :
			bounds = ([-2, -np.inf, 0, 0], [2, 0, np.inf, np.inf])
			popt, pcov = curve_fit(voigt, velocity, line, maxfev = 2000, bounds = bounds)
			residuals = line - voigt(velocity, *popt)
			new_res, indexes = recursive_clipping(residuals, Nsigma*np.nanstd(residuals))

			for i in indexes[::-1] :
				line[i] = np.nan
				velocity = velocity[~np.isnan(line)]
				line = line[~np.isnan(line)]
			WRONGS = indexes
		subset.append(line)
		subvel.append(velocity)
	### end of cleaning loop
	#print(len(subset), len(subvel), len(freqs))
    
	print("End : CLEANING phase")

	# Formatting : make each slide the same size (width_of_line channel-broad)
	print("Beginning : FORMATTING phase")
    
	new_S   = []
	new_vel = []
	for n in range(len(subset)) :
		l = subset[n]
		v = subvel[n]
		f  = freqs[n]
		f0 = np.nanmean(f)

		l_ref = np.zeros(width_of_line)
		v_ref = f_to_v(np.arange(-df*(width_of_line//2),df*(width_of_line//2),df), f0).value/1e3
		for i in range(width_of_line):
			if v_ref[i] in v :
				index = np.where(v == v_ref[i])[0]
				l_ref[i] = l[index]
			else :
				l_ref[i] = np.nan
		new_S.append(l_ref)
		new_vel.append(v_ref)
	### end of reconstructing loop  
    
	print("End : FORMATTING phase")

	# Stack the different lines 

	if len(new_S)!=0:
		stack = np.nanmean(new_S, axis = 0)
		print('Beginning : FITTING phase')

		f0   = np.nanmean(f)
		freq = np.arange(-df*(width_of_line//2), df*(width_of_line//2), df)
		velocity = f_to_v(freq, f0).value[~np.isnan(stack)]/1e3
		final_line = stack[~np.isnan(stack)]
		popt, pcov, fct, convos = voigt_fitting_FINAL(velocity, final_line, velocities, f0, df, turbu) 
	
		print('End : FITTING phase')
    
	else:
    		print("No line expected in the Band")
    		continue

	# extract data from fit :

	### popt = center, *amplitudes, *lorentz_widths
        #
	opt_center  = popt[0] 
	opt_amp = [popt[1]] + [popt[1]*popt[i] for i in range(2, nb+1)]
	opt_lw  = popt[nb+1:]
	opt_gw = convos
	centers = [opt_center + velocities[i] for i in range(nb)]
	MODELS = []
	MODEL  = multiple_voigt(velocity, centers, opt_amp, opt_lw, opt_gw)
	for i in range(nb):
    		single_model = voigt(velocity, centers[i], opt_amp[i], opt_lw[i], opt_gw[i])
    		MODELS.append(single_model)

	# Overplot the voigt fit

	LINE = HDU.select_lines(myLine, band)
	n_quantum_moy = int(np.mean(LINE.index))
	n_quantum_min = np.min(LINE.index)
	n_quantum_max = np.max(LINE.index)

	residuals = final_line - MODEL
	
	noise = residuals.std()
	
	plt.figure()
	plt.xlim(-150, 150)

	plt.plot(velocity, final_line)
	plt.plot(velocity, MODEL, label='all')
	for i in range(nb) :
		plt.plot(velocity, MODELS[i], '--', label=f'comp n°{i}')


	plt.plot(velocity, residuals)
	plt.title(f'Stacked bewteen n {n_quantum_min} and {n_quantum_max} \n Tau in Band {band} for {myLine} rms : {noise : .2e}')
	plt.xlabel("Centered Velocity (km/s)")
	plt.ylabel("Stacked Tau")

	try :
		calpha=HDU.select_lines('Calph',band).values[0]
		cbeta=HDU.select_lines('Cbeta',band).values[0]
		cbeta_v_shift=f_to_v((cbeta-calpha), HDU.get_freq(band)).to(u.km/u.s)
		if np.abs(cbeta_v_shift.value) > deltav :
			print(f"Cbeta far from Calpha : shift = {cbeta_v_shift.value : .2f} km/s")
		else :
			plt.vlines(cbeta_v_shift.value, np.min(final_line), np.max(final_line), label="Cbeta", color = 'black')
			print(f"shift = {cbeta_v_shift.value : .2f} km/s")
	except:
		print("No Cbeta in Band")
		pass

	plt.legend()

	plt.savefig(parent + f"{data.split('/')[0]}/PNG/Band-"+str(band)+"-stack-"+myLine+f"-lane-{Lane}.png")

	# uncertainties
	perr = np.sqrt(np.diag(pcov))
	Delta_centers = [perr[0]]*nb
	Delta_amp = perr[1:nb+1]
	Delta_lw  = perr[nb+1:]
	Delta_gw  = [0]*nb	
	
	## all infos
	MINIMAs = [np.min(MODELS[i]) for i in range(nb)]

	SNRs = np.abs(np.array(MINIMAs))/noise

	FWHMs = [fwhm_fct(opt_lw[i], opt_gw[i]) for i in range(nb)]

	FWHM_ERRs = [fwhm_voigt_err(Delta_lw[i], Delta_gw[i], opt_lw[i], opt_gw[i]) for i in range(nb)]

	GAUSS_FRACTIONs = [opt_gw[i]/(opt_gw[i] + opt_lw[i]) for i in range(nb)]

	LINE_AREAs = [v_to_f(voigt_area(MINIMAs[i], opt_lw[i], opt_gw[i]), freq_band).to(u.Hz)]

	CBETA_FLAGs = np.zeros(nb, dtype = bool)
	for i in range(nb):
		fwhm = FWHMs[i]
		fwhm_err = FWHM_ERRs[i]
		try :
			if (abs(cbeta_v_shift.value) < abs(popt[0]-fwhm/2-3*fwhm_err)):
				CBETA_FLAGs[i] = True
				print(f"True : blending by Cbeta for component n°{i}")
		except :
			print("No Cbeta")
			pass


	# Write Tau, Signal and rms in a dictionary

	d1 = {'Band':band, 'freq_band':freq_band*u.MHz, 'Line':myLine, "velocity": velocity, "final_line": stack, "residuals": residuals, 
	'velocities':velocities, 'velo_0': velos_['velo_0'], 'popt':popt, 'pcov':pcov, 
	'n_quantum_moy':n_quantum_moy, 'n_quantum_min':n_quantum_min, 'n_quantum_max':n_quantum_max,
     	'SNR': SNRs, 'LINE_AREA' : LINE_AREAs, 'GAUSS_FRACTION': GAUSS_FRACTIONs, 'FWHM':np.array(FWHMs)*u.km/u.s, 'FWHM_ERR':np.array(FWHM_ERRs)*u.km/u.s, 
	'FWHM_freq':abs(v_to_f(np.array(FWHMs), HDU.get_freq(band))), 'FWHM_ERR_freq':abs(v_to_f(np.array(FWHM_ERRs), HDU.get_freq(band))), 
	'FWHM_gauss':np.array(opt_gw)*u.km/u.s, 'FWHM_gauss_err':np.array(Delta_gw)*u.km/u.s, 'FWHM_gauss_freq':abs(v_to_f(np.array(opt_gw),freq_band)), 
	'FWHM_gauss_freq_err':abs(v_to_f(np.array(Delta_gw), freq_band)), 'CBETA_FLAG':CBETA_FLAGs, "df" : df, "turbu" : turbu, "velocities" : velocities, "velo_1" : velo_1, 'width_of_line' : width_of_line, 'myLine' : myLine}

	with open(parent + f"{data.split('/')[0]}/pickles/Band-{band}-stack-{myLine}-lane-{Lane}.pickle", "wb") as output_file:
		pickle.dump(d1, output_file)
end = time.time()
print(f"processing time = {(end- start)/60} min")
