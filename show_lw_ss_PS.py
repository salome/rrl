#!/usr/bin/env python

import numpy as np
import pylab as plt
import astropy.units as u
import pickle

from crrlpy import crrls
from crrlpy import utils
from crrlpy.models import rrlmod
from crrlpy import frec_calc as fc
from astropy.io import ascii
from astropy.table import vstack
from lmfit import Model
#from snu_cas import main

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
rc('font', weight='bold')
import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'

rotated_labels = []
def text_slope_match_line(text, x, y, line):
    global rotated_labels

    # find the slope
    xdata, ydata = line.get_data()

    x1 = xdata[0]
    x2 = xdata[-1]
    y1 = ydata[0]
    y2 = ydata[-1]

    rotated_labels.append({"text":text, "line":line, "p1":np.array((x1, y1)), "p2":np.array((x2, y2))})

def update_text_slopes():
    global rotated_labels

    for label in rotated_labels:
        # slope_degrees is in data coordinates, the text() and annotate() functions need it in screen coordinates
        text, line = label["text"], label["line"]
        p1, p2 = label["p1"], label["p2"]

        # get the line's data transform
        ax = line.get_axes()

        sp1 = ax.transData.transform_point(p1)
        sp2 = ax.transData.transform_point(p2)

        rise = (sp2[1] - sp1[1])
        run = (sp2[0] - sp1[0])

        slope_degrees = np.rad2deg(np.arctan(rise/run))

        text.set_rotation(slope_degrees)

def fit_lw(n, dL, ddL, te, ne, tr, w):
    """
    Simple radiation (Galactic only) and pressure broadening.
    """
    
    a, gc = crrls.pressure_broad_coefs(te)
    pressure_broad_comp = lambda n, ne: ne*np.power(10., a)*np.power(n, gc)/np.pi
    mod_r = Model(crrls.radiation_broad_salgado)
    mod_p = Model(pressure_broad_comp)
    mod = mod_r + mod_p
    pars = mod.make_params(Tr=tr, W=w, ne=ne)
    pars['W'].set(vary=False, value=w)
    pars['ne'].set(vary=False, value=ne, min=0.0)
    pars['Tr'].set(vary=True, value=tr, min=0.0)
    
    fit_c = mod.fit(dL, pars, 1/ddL, n=n)
    
    return fit_c

def fit_lw_tr(n, dL, ddL, te, ne, tr, w):
    """
    Only radiation broadening.
    """
    
    mod_r = Model(crrls.radiation_broad_salgado)
    mod = mod_r
    pars = mod.make_params(Tr=tr, W=w, ne=ne)
    pars['W'].set(vary=False, value=w)
    pars['Tr'].set(vary=False, value=2800.0, min=0.0)
    
    fit_c = mod.fit(dL, pars, 1/ddL, n=n)
    
    return fit_c

def fit_lw_bpl(n, dL, ddL, te, ne, nu0):
    """
    Broken power law (Cassiopeia A with absorption).
    """
    
    a, gc = crrls.pressure_broad_coefs(te)
    pressure_broad_comp = lambda n, ne: ne*np.power(10., a)*np.power(n, gc)/np.pi
    mod_p = Model(pressure_broad_comp)
    userrad = lambda n, tr, nu0, alpha1: rrlmod.I_Bnu('CI', 1.,  n, rrlmod.I_broken_plaw, tr, nu0*u.MHz.to('Hz'), alpha1, -2.6)
    mod_usrrad = Model(userrad)
    mod = mod_p + mod_usrrad
    pars = mod.make_params(ne=ne, tr=12e3, nu0=26, alpha1=-1.)
    pars['tr'].set(value=12e3, min=0.0, vary=True)
    pars['nu0'].set(value=26, min=0.0, vary=False)
    pars['alpha1'].set(value=-1, vary=True)
    pars['ne'].set(value=ne, min=0.0, vary=False)
    fit = mod.fit(dL, pars, 1/ddL, n=n)
    
    return fit

def fit_lw_all(n, dL, ddL, te, ne, nu0, tr):
    """
    Pressure and radiation broadening.
    For radiation broadening we include a 
    power law (Galactic synchrotron) and a 
    broken power law (Cassiopeia A).
    """
    w = 1
    a, gc = crrls.pressure_broad_coefs(te)
    pressure_broad_comp = lambda n, ne: ne*np.power(10., a)*np.power(n, gc)/np.pi
    mod_p = Model(pressure_broad_comp)
    userrad = lambda n, tr, nu0, alpha1: rrlmod.I_Bnu('CI', 1.,  n, rrlmod.I_broken_plaw, tr, nu0*u.MHz.to('Hz'), alpha1, -2.6)
    mod_usrrad = Model(userrad)
    mod_r = Model(crrls.radiation_broad_salgado)
    mod = mod_p + mod_usrrad + mod_r
    pars = mod.make_params(ne=ne, tr=12e3, nu0=26, alpha1=-1., Tr=800, W=1)
    pars['tr'].set(value=12e3, min=0.0, vary=True)
    pars['nu0'].set(value=26, min=0.0, vary=False)
    pars['alpha1'].set(value=-1, vary=False)
    pars['ne'].set(value=ne, min=0.0, vary=False)
    pars['W'].set(value=w, vary=False)
    pars['Tr'].set(value=800.0, min=0.0, vary=False)
    fit = mod.fit(dL, pars, 1/ddL, n=n)
    
    return fit
    

if __name__ == '__main__':
    
    dD = 3.4 # Doppler line width, km/s.
    
    n_min = 400
    n_max = 1050
    ncut = 700 # Points above were fitted with 1 Voigt.
    
    Te = 80
    ne = 0.04
    Tr = 1400
    W = 1.
    nu0 = 26
    Te_bpl = 80
    
    # Load the data.
#PS    data0 = ascii.read('../LOFAR_fits/LBA_all/CIalpha_-47kms_nomod_dop.log', 
#PS                       guess=False, format='fixed_width',
#PS                       header_start=0, delimiter='|')
#PS    data1 = ascii.read('../LOFAR_fits/LBA_all/CIalpha_-38kms_nomod_dop.log', 
#PS                       guess=False, format='fixed_width',
#PS                       header_start=0, delimiter='|')
#PS    data2 = ascii.read('../LOFAR_fits/LBA_all/CIalpha_-47kms_nomod_dop.log', 
##PS    data2 = ascii.read('../LOFAR_fits/LBA_all/CIalpha_-47kms_nomod_1c.log',
#PS                       guess=False, format='fixed_width',
#PS                       header_start=0, delimiter='|')
##PS    data3 = ascii.read('../LOFAR_fits/LBA_all/CIbeta_-47kms_nomod_1c.log',
##PS                       guess=False, format='fixed_width',
##PS                       header_start=0, delimiter='|')
    
    stepk = np.loadtxt('stepkin+07.txt')#[:-2]
    ns = (stepk[:,0] + stepk[:,1])/2.
    dLs = np.empty(len(ns))
    dLs2 = np.empty(len(ns))
    ddLs = np.empty(len(ns))
    ddLs2 = np.empty(len(ns))
    fs = np.empty(len(ns))
    for i,n in enumerate(ns):
        dLs[i], ddLs[i] = crrls.dv_minus_doppler(stepk[i,2], stepk[i,3], 3.2, 0.)
        dLs2[i], ddLs2[i] = crrls.dv_minus_doppler2(stepk[i,2], stepk[i,3], 3.2, 0.)
        fs[i] = crrls.n2f(round(n), 'CI'+fc.set_trans(i+1))
    dLsf = crrls.dv2df(fs*1e6, dLs*1e3)
    dLsf2 = crrls.dv2df(fs*1e6, stepk[:,2]*1e3)
    ddLsf2 = crrls.dv2df(fs*1e6, stepk[:,3]*1e3)
    ddLsf = crrls.dv2df(fs*1e6, stepk[:,3]*1e3)

    #########
    ## Separate the data into LBA low and the rest
    #PS cut = np.where(data2['n'].data > 650)
    #PS data2 = data2[cut]
    #PS #cut = np.where(data0['n'].data > 600 and data0['n'].data < 650)
    #PS cut = (data0['n'].data > 600) & (data0['n'].data < 650)
    #PS data2 = vstack((data0[cut], data2))
    #PS cut = np.where(data0['n'].data < 600)
    #PS data0 = data0[cut]
    #PS data1 = data1[cut]
    
    #PS dL2c = crrls.dv2df(data0['f0 (MHz)'].data*1e6, data0['FWHM (km/s)']*1e3)
    #PS ddL2c = crrls.dv2df(data0['f0 (MHz)'].data*1e6, data0['FWHM_err (km/s)']*1e3)
    #PS dL1c = crrls.dv2df(data2['f0 (MHz)'].data*1e6, data2['FWHM (km/s)']*1e3)
    #PS ddL1c = crrls.dv2df(data2['f0 (MHz)'].data*1e6, data2['FWHM_err (km/s)']*1e3)
    #PS ###dLb = crrls.dv2df(data3['f0 (MHz)'].data*1e6, data3['FWHM (km/s)']*1e3)
    #PS ###ddLb = crrls.dv2df(data3['f0 (MHz)'].data*1e6, data3['FWHM_err (km/s)']*1e3)
    
    ## Concatenate
    #PS n = np.concatenate((data0['n'].data, data2['n'].data))
    #PS dL = np.concatenate((dL2c, dL1c))
    #PS #print dL
    #PS ddL = np.concatenate((ddL2c, ddL1c))
    #PS f = crrls.n2f(n, 'RRL_CIalpha')
    #########
    
    # Compute various models for the line broadening
    nss = np.arange(n_min, n_max)
    
    # Convert the Doppler line width to frequency
    fmod = crrls.n2f(nss, 'RRL_CIalpha')
    dD_fa = crrls.dv2df(fmod*1e6, dD*1e3) # Hz
    dD_fa_s = crrls.dv2df(fmod*1e6, 15.8*1e3) # Hz
    
    ## Define the Cassiopeia A line width
    #line_width = lambda n, omega, dD: crrls.voigt_fwhm(dD, crrls.pressure_broad_salgado(n, 85., 0.04)+
                                                       #crrls.radiation_broad_salgado(n, 1., 800.0)+
                                                       #omega*rrlmod.I_Bnu('RRL_CI', 1.,  n, main))
    
    elw = 2
    
    fig = plt.figure(frameon=False, figsize=(5,5))
    ax = fig.add_subplot(1, 1, 1, adjustable='datalim')
    ax1 = ax.twiny()
    
    #PS (_, caps, _) = ax.errorbar(n, dL, yerr=ddL, fmt='.', c='r', ms=9, barsabove=True, capsize=6, label=r'C$\alpha$', elinewidth=elw)
    #PS (_, caps, _) = ax.errorbar(data2['n'][:-1], dL1c[:-1]/1e3, yerr=ddL1c[:-1]/1e3, fmt='.', c='r', ms=9, barsabove=True, capsize=6, label=r'C$\alpha$', elinewidth=elw, zorder=20)
    #PS for cap in caps:
    #PS     cap.set_markeredgewidth(elw)
        
    # Open symbols
    #PS (_, caps, _) = ax.errorbar(data2['n'][-3:-1], dL1c[-3:-1]/1e3, yerr=ddL1c[-3:-1]/1e3, fmt='.', c='r', mfc='w', mec='r', ms=9, barsabove=True, capsize=6, label=r'C$\alpha$', elinewidth=elw, zorder=20)
    #PS for cap in caps:
    #PS     cap.set_markeredgewidth(elw)

    #(_, caps, _) = ax.errorbar(data3['n'][-1], dLb[-1]/1e3, yerr=ddLb[-1]/1e3, c='orange', fmt='.', ms=9, barsabove=True, capsize=6, label=r'C$\beta$', elinewidth=elw, zorder=19)
    #for cap in caps:
        #cap.set_markeredgewidth(elw)
        
    #PS ax.errorbar(data0['n'].data, dL2c/1e3, yerr=ddL2c/1e3, c='r', fmt='x', ms=7, mec='r', label='LCASS -47 km s$^{-1}$', barsabove=True, capsize=6, zorder=18)
    
    ax.errorbar(ns, dLsf/1e3, yerr=ddLsf/1e3, fmt='s', c='purple', label='Stepkin+07', capsize=6)
    
    line, = ax.plot(nss, dD_fa/1e3, 'y-')
    indx = 45
    txt = ax.text(nss[indx], dD_fa[indx]/1e3*0.85, '{0} km s$^{{-1}}$'.format(dD), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=-28, color='y')
    text_slope_match_line(txt, nss[indx], dD_fa[indx]/1e3*0.85, line)
    
    line, = ax.plot(nss, dD_fa_s/1e3, 'y--')
    indx = 45
    txt = ax.text(nss[indx], dD_fa_s[indx]/1e3*0.85, '{0} km s$^{{-1}}$'.format(15), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=-28, color='y')
    text_slope_match_line(txt, nss[indx], dD_fa_s[indx]/1e3*0.85, line)
    
    # Radiation field
    color = '#332288'
    dL_r1 = crrls.radiation_broad_salgado(nss, 1., 800.0)/1e3
    line, = ax.plot(nss, dL_r1, c=color, ls='-')
    indx = 250
    txt = ax.text(nss[indx], dL_r1[indx]*0.82, '${0}$ K'.format(800), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=44, color=color)
    text_slope_match_line(txt, nss[indx], dL_r1[indx]*0.82, line)
    
    dL_r2 = crrls.radiation_broad_salgado(nss, 1., 100.0)/1e3
    line, = ax.plot(nss, dL_r2, c=color, ls='--')
    indx = 500
    txt = ax.text(nss[indx], dL_r2[indx]*0.80, '$T_{{r,100}}={0}$ K'.format(100), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=44, color=color)
    text_slope_match_line(txt, nss[indx], dL_r2[indx]*0.80, line)
    
    # Electron density
    dL_p1 = crrls.pressure_broad_salgado(nss, 90., 0.04)/1e3
    line, = ax.plot(nss, dL_p1, c='#AA4499', ls='-')
    indx = 350
    txt = ax.text(nss[indx], dL_p1[indx]*1.15, '$({0},{1})$'.format(90, 0.04), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=43, color='#AA4499')
    text_slope_match_line(txt, nss[indx], dL_p1[indx]*1.15, line)
    
    dL_p3 = crrls.pressure_broad_salgado(nss, 90., 0.01)/1e3
    line, = ax.plot(nss, dL_p3, c='#AA4499', ls='-')
    indx = 400
    txt = ax.text(nss[indx], dL_p3[indx]*1.15, '$(T_{{e}},n_{{e}})=({0},{1})$ K,cm$^{{-3}}$'.format(90, 0.01), 
                  #'$(T_{{e}},n_{{e}})=({0},{1})$ K,cm$^{{-3}}$'.format(90, 0.01), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=43, color='#AA4499')
    text_slope_match_line(txt, nss[indx], dL_p3[indx]*1.15, line)
    
    dL_p5 = crrls.pressure_broad_salgado(nss, 500., 0.01)/1e3
    line, = ax.plot(nss[::20], dL_p5[::20], c='#AA4499', ls='-.')
    indx = 450
    txt = ax.text(nss[indx], dL_p5[indx]*1.18, '$({0},{1})$'.format(500, 0.01),
                  #'$(T_{{e}},n_{{e}})=({0},{1})$ K,cm$^{{-3}}$'.format(500, 0.01), 
                  horizontalalignment='center', verticalalignment='center', size=10,
                  transform=ax.transData, rotation=43, color='#AA4499')
    text_slope_match_line(txt, nss[indx], dL_p5[indx]*1.18, line)
    
    color = '#117733'
    ax.plot(nss, crrls.voigt_fwhm(dD_fa, crrls.pressure_broad_salgado(nss, 85., 0.04)+
                                  crrls.radiation_broad_salgado(nss, 1., 1400.0))/1e3, c=color, ls='-.')
    ax.plot(nss, crrls.voigt_fwhm(dD_fa, crrls.pressure_broad_salgado(nss, 85., 0.04)+
                                  crrls.radiation_broad_salgado(nss, 1., 800.0)+
                                  rrlmod.I_Bnu('RRL_CI', 1.,  nss, 
                                               rrlmod.I_broken_plaw, 
                                               12000, 26*u.MHz.to('Hz'), -1., -2.6))/1e3, c=color, ls=':')
    omega = 5e-5#1.8943e-05
    #ax.plot(nss, line_width(nss, omega, crrls.dv2df(crrls.n2f(nss, 'RRL_CIalpha')*1e6, 
                                                    #data0['FWHM_gauss (km/s)'][0]*1e3))/1e3, c=color, ls='-')
    
    # Stepkin et al. 2007
    ax.plot(nss, crrls.voigt_fwhm(dD_fa_s, crrls.pressure_broad(nss, 75., 0.02) +
                                  crrls.radiation_broad(nss, 1., 800.0) + 
                                  rrlmod.I_Bnu('RRL_CI', 1.,  nss, 
                                               rrlmod.I_broken_plaw, 
                                               12000, 26*u.MHz.to('Hz'), -1., -2.6))/1e3, c='#88CCEE', ls='--')
    
    #ax.minorticks_on()
    ax.set_xlabel(r'Principal quantum number')
    ax.set_ylabel(r'Line width (kHz)')
    #plt.semilogy()
    #plt.semilogx()
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax1.set_xscale('log')
    ax.set_xlim(round(nss[0],-2), nss[-1])
    plt.tick_params(axis='y', which='minor')
    ax.xaxis.set_minor_formatter(plt.matplotlib.ticker.FormatStrFormatter("%.0f"))
    ax.xaxis.set_major_formatter(plt.matplotlib.ticker.FormatStrFormatter("%.0f"))
    ax.set_ylim(0.380, 40)
    nbins = len(ax.get_yticklabels())
    #ax.yaxis.set_major_locator(plt.matplotlib.ticker.MaxNLocator(nbins=nbins, prune='lower'))
    
    ax.legend(loc=2, numpoints=1, frameon=False, fontsize='medium')
    
    # Top axis
    qns, rfreq =  crrls.load_ref('RRL_CIalpha')
    ax1.set_xlabel(r'C$\alpha$ frequency (MHz)')
    ax1.set_xlim(round(nss[0],-2), nss[-1])
    ax1.minorticks_on()
    nbins = len(ax.get_xticklabels())
    xticks = np.concatenate((ax.xaxis.get_minorticklocs()[10:16], [1000]))
    xticklabs = ax.xaxis.get_minorticklabels()
    #topx = []
    #for xticklab in xticklabs:
        #print xticklab.get_position()[0]
        #if xticklab.get_position()[0] != 0:
            #print 'appending'
            #topx.append(xticklab.get_position()[0])
    topxticks = np.zeros(len(xticks))
    for i,xtick in enumerate(xticks):
        topxticks[i] = int(round(rfreq[utils.best_match_indx(xtick, qns)]))
    ax1.set_xticks(xticks)
    ax1.set_xticklabels(topxticks)
    
    path = f'../script/Band-linewidth-.pickle'
    with open(path, 'rb') as h :
        data = pickle.load(h)

    ### extracting data
    quantum = data['quantum']
    linewidth = data['linewidth']

    ax.plot(quantum, linewidth, 'b.')

    #update_text_slopes()
    plt.show()
    plt.savefig('linewidth_ss2.pdf',
                bbox_inches='tight')
    plt.close(fig)
    
