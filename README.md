Python pipeline to reduce NenuFAR observations of RRL (Radio Recombination Line) for the Early Science ES10 project

Use: 

python Pipeline_stacking_fitting.py "/obs/salome/ES10/es10-share/DATA/" "CAS_A_TRACKING_20191005_003136/CAS_A_TRACKING_20191005_003136_0.spectra.fits" 0 -47 [0,9,47] [2,2,2.5] 'Calph' 3 400 0.8 50000 25 75 80

Average on-time
python average_on_time.py "/obs/lcros/ES10/es10-share" "/DATA-CASA/" 192 myLine

Notice  Global_stacking
Global_stacking.py k1 k2 "/obs/lcros/ES10/" "DATA/CAS_A_TRACKING_20210919_230045" Lane myLine
exemple :
Global_stacking.py 15 25 "/obs/lcros/ES10/" "DATA/CAS_A_TRACKING_20210919_230045" 0 'Calph'

inputs :
k1, k2 : [int,int] stacks from subband k1 to subband k2
parent path : [str] in which is the /Processed-data folder
data path : [str] path to the main file of data, containing .fits AND /pickles, /PNG
Lane : [int] lane to process


Team : 
Lucie Cros
Antoine Gusdorf
Jonathan Freundlich
Philippe Salomé
