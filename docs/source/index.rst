.. RRL documentation master file, created by
   sphinx-quickstart on Mon Nov  8 10:21:00 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================
NenuFAR RRL Pipeline
====================
---------------------------------
Radio-Recombination Line Pipeline
---------------------------------

Introduction
============

This project documents the pipeline for data reduction of Radio Recombination Lines observed by NenuFAR in the
Early-Science ES10 project. The main procedure is a pipeline that calls the L1 and calcul class fonction.

Outputs
----------

The products are png files and pickles output.

- Python pickles
- PNG plots

===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
