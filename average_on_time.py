#!/usr/bin/env python
# coding: utf-8


from calcul import *
from astropy.io import fits
import sys
import pickle
from astropy.modeling.models import Voigt1D , Gaussian1D
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
import os
import time
## GET INFOS FROM USER
start = time.time()

parent = sys.argv[1]
print('parent_path :' + parent)
data = sys.argv[2]
print(f'data_path : {data}')
nb_channels = int(sys.argv[3])
print(f'nb_channels = {nb_channels}')
myLine = sys.argv[4]

paths = os.listdir(parent + data)
for Lane in range(2):
    
    for band in range(nb_channels):
        print(f'AVERAGING for subband n°{band}')
        sub_stack = []
        FREQS = []
        weights = []
        print('Beginning : STACKING phase')
        for path in paths :
            try :
                with open(parent + data + f"{path}/pickles/Band-{band}-stack-{myLine}-lane-{Lane}.pickle", "rb") as input_file:
                    d1 = pickle.load(input_file)
                final_line = d1['final_line']
                sub_stack.append(final_line)
                FREQS.append(d1['freq_band'].value)
                df = d1['df']
                turbu = d1["turbu"]
                velocities = d1["velocities"]
                velo_1 = d1["velo_1"]
                width_of_line = d1['width_of_line']
                noise = d1['residuals'].std()
                weights.append(1/noise**2)
                nb = len(velocities)
                
            except :
                pass
        
        # Get the general data
        #weights = np.array(weights)/np.nansum(weights)
        if len(sub_stack) == 0 :
            print(f"no data in subband n°{band}")
            continue
        
        weights = np.array(weights)/np.nansum(weights)
        weighted = np.multiply(sub_stack, np.array([weights]).T)
        avg_subband = np.nansum(weighted, axis = 0)

        f0   = np.nanmean(FREQS)
        freq_band = f0
        freq = np.arange(-df*(width_of_line//2), df*(width_of_line//2), df)
        velocity = f_to_v(freq, f0).value[~np.isnan(avg_subband)]/1e3
        velos_ = dict()
        for i in range(len(velocities)) :
            exec(f'velos_["velo_{i}"] = {velocities[i]} + velo_1')
        print('Ending : STACKING phase')
        print('Beginning : FITTING phase')

        final_line = avg_subband[~np.isnan(avg_subband)]
        popt, pcov, fct, convos = voigt_fitting_FINAL(velocity, final_line, velocities, f0, df, turbu) 
        print('End : FITTING phase')

        # extract data from fit :

        ### popt = center, *amplitudes, *lorentz_widths

        opt_center  = popt[0]
        opt_amp = [popt[1]] + [popt[1]*popt[i] for i in range(2, nb+1)]
        #print(opt_amp)
#        opt_amp = popt[1:nb+1]
        opt_lw  = popt[nb+1:]
        opt_gw = convos

        centers = [opt_center + velocities[i] for i in range(nb)]

        MODELS = []
        MODEL  = multiple_voigt(velocity, centers, opt_amp, opt_lw, opt_gw)
        for i in range(nb):
            single_model = voigt(velocity, centers[i], opt_amp[i], opt_lw[i], opt_gw[i])
            MODELS.append(single_model)

        # Overplot the voigt fit

        n_quantum_moy = d1['n_quantum_moy']
        n_quantum_min = d1['n_quantum_min']
        n_quantum_max = d1['n_quantum_max']

        residuals = final_line - MODEL
        noise = residuals.std()

        plt.figure()
        plt.xlim(-150, 150)

        plt.plot(velocity, final_line)
        plt.plot(velocity, MODEL, label='all')
        for i in range(nb) :
            plt.plot(velocity, MODELS[i], '--', label=f'comp n°{i}')


        plt.plot(velocity, residuals)
        plt.title(f'Stacked bewteen n {n_quantum_min} and {n_quantum_max} \n Tau in Band {band} for {myLine} rms : {noise : .2e}')
        plt.xlabel("Centered Velocity (km/s)")
        plt.ylabel("Stacked Tau")

        

        plt.legend()

        plt.savefig(parent + f"Processed_data/{data}AVERAGE/PNG/Band-"+str(band)+"-stack-"+myLine+f"-lane-{Lane}.png")

        # uncertainties
        perr = np.sqrt(np.diag(pcov))
        Delta_centers = [perr[0]]*nb
        Delta_amp = perr[1:nb+1]
        Delta_lw  = perr[nb+1:]
        Delta_gw  = [0]*nb
        ## all infos
        MINIMAs = [np.min(MODELS[i]) for i in range(nb)]

        SNRs = np.abs(np.array(MINIMAs))/noise

        FWHMs = [fwhm_fct(opt_lw[i], opt_gw[i]) for i in range(nb)]

        FWHM_ERRs = [fwhm_voigt_err(Delta_lw[i], Delta_gw[i], opt_lw[i], opt_gw[i]) for i in range(nb)]

        GAUSS_FRACTIONs = [opt_gw[i]/(opt_gw[i] + opt_lw[i]) for i in range(nb)]

        LINE_AREAs = [v_to_f(voigt_area(MINIMAs[i], opt_lw[i], opt_gw[i]), freq_band).to(u.Hz)]

    # Write Tau, Signal and rms in a dictionary

        d_1 = {'Band':band, 'freq_band':freq_band*u.MHz, 'Line':myLine, "velocity": velocity, "final_line": avg_subband, "residuals": residuals, 
    'velocities':velocities, 'velo_1': velo_1, 'popt':popt, 'pcov':pcov, 
        'n_quantum_moy':n_quantum_moy, 'n_quantum_min':n_quantum_min, 'n_quantum_max':n_quantum_max,
        'SNR': SNRs, 'LINE_AREA' : LINE_AREAs, 'GAUSS_FRACTION': GAUSS_FRACTIONs, 'FWHM':np.array(FWHMs)*u.km/u.s, 'FWHM_ERR':np.array(FWHM_ERRs)*u.km/u.s, 
        'FWHM_freq':abs(v_to_f(np.array(FWHMs), f0)), 'FWHM_ERR_freq':abs(v_to_f(np.array(FWHM_ERRs), f0)), 
        'FWHM_gauss':np.array(opt_gw)*u.km/u.s, 'FWHM_gauss_err':np.array(Delta_gw)*u.km/u.s, 'FWHM_gauss_freq':abs(v_to_f(np.array(opt_gw),freq_band)), 
        'FWHM_gauss_freq_err':abs(v_to_f(np.array(Delta_gw), freq_band)), "df" : df, "turbu" : turbu, "width_of_line" : width_of_line, "noise" : noise}

        
        ### Band-{band}-stack-{myLine}-lane-{Lane}.pickle
        with open(parent + f"Processed_data/{data}AVERAGE/pickles/Band-{band}-stack-{myLine}-lane-{Lane}.pickle", "wb") as output_file:
            pickle.dump(d_1, output_file)

end = time.time()
print(f"processing time = {(end-start)/60} min")
