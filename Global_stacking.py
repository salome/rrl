#!/usr/bin/env python
# coding: utf-8


from calcul import *
from astropy.io import fits
import sys
import pickle
from astropy.modeling.models import Voigt1D , Gaussian1D
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
import os

### GET INFO FROM USER
band_inf = int(sys.argv[1])
band_sup = int(sys.argv[2])

parent = sys.argv[3]
path = sys.argv[4]
Lane = int(sys.argv[5])
myLine = sys.argv[6]
#### STACKING TIME
sub_stack = []
FREQS = []
global_path = parent + path
weights = []
SUM = 0
for band in range(band_inf, band_sup):
	
	path =  f"{global_path}/pickles/Band-{band}-stack-{myLine}-lane-{Lane}.pickle"
	try :
		with open(path, 'rb') as h :
			d1 = pickle.load(h)
	
		final_line = d1['final_line']
		sub_stack.append(final_line)
		FREQS.append(d1['freq_band'].value)
		df = d1['df']
		turbu = d1["turbu"]
		velocities = d1["velocities"]
		velo_1 = d1["velo_1"]
		width_of_line = d1['width_of_line']
		noise = d1['residuals'].std()
		weights.append(1/noise**2)
		SUM+= 1/noise**2
	except :
		continue
if len(sub_stack)==0 :
	print("PROBLEM WITH LOOP")
velos_ = dict()
for i in range(len(velocities)) :
	exec(f'velos_["velo_{i}"] = {velocities[i]} + velo_1')
nb = len(velocities)

weights = np.multiply(np.ones((len(sub_stack[0]),len(sub_stack))),np.array(weights))/SUM
print(weights.T[:5,:5])
print(np.array(sub_stack)[:5,:5])

weighted = np.multiply(sub_stack, weights.T)
#stack_subband_2 = np.nansum(weighted, axis = 0)
#print(np.array(sub_stack).shape, weights.T.shape)
stack_subband = np.average(np.array(sub_stack), axis = 0, weights = weights.T)
#stack_no_weight = np.sum(sub_stack, axis = 0)/157
#stack_no_weight = np.average(np.array(sub_stack), axis = 0, weights = np.ones(weights.T.shape))
#print(np.sum(weights/np.nansum(weights)))
f0   = np.nanmean(FREQS)
freq_band = f0
freq = np.arange(-df*(width_of_line//2), df*(width_of_line//2), df)
velocity = f_to_v(freq, f0).value[~np.isnan(stack_subband)]/1e3

print('Ending : STACKING phase')
print('Beginning : FITTING phase')

final_line = stack_subband[~np.isnan(stack_subband)]
popt, pcov, fct, convos = voigt_fitting_FINAL(velocity, final_line, velocities, f0, df, turbu) 
print('End : FITTING phase')

# extract data from fit :

### popt = center, *amplitudes, *lorentz_widths

opt_center  = popt[0]
opt_amp = [popt[1]] + [popt[1]*popt[i] for i in range(2, nb+1)]
#opt_amp = popt[1:nb+1]
opt_lw  = popt[nb+1:]
opt_gw = convos

centers = [opt_center + velocities[i] for i in range(nb)]

MODELS = []
MODEL  = multiple_voigt(velocity, centers, opt_amp, opt_lw, opt_gw)
for i in range(nb):
	single_model = voigt(velocity, centers[i], opt_amp[i], opt_lw[i], opt_gw[i])
	MODELS.append(single_model)

# Overplot the voigt fit

n_quantum_moy = d1['n_quantum_moy']
n_quantum_min = d1['n_quantum_min']
n_quantum_max = d1['n_quantum_max']

residuals = final_line - MODEL
noise = residuals.std()

plt.figure()
plt.xlim(-150, 150)

plt.plot(velocity, final_line)
plt.plot(velocity, MODEL, label='all')
for i in range(nb) :
	plt.plot(velocity, MODELS[i], '--', label=f'comp n°{i}')


plt.plot(velocity, residuals)
plt.title(f'Stacked bewteen n {n_quantum_min} and {n_quantum_max} \n Tau between subbands {band_inf} and {band} for {myLine} rms : {noise : .2e}')
plt.xlabel("Centered Velocity (km/s)")
plt.ylabel("Stacked Tau")

plt.legend()

plt.savefig(parent + f"/STACKING/PNG/from-subband-{band_inf}-to-{band_sup}-stack-{myLine}-lane-{Lane}-date-{global_path.split('/')[-1]}.png")

# uncertainties
perr = np.sqrt(np.diag(pcov))
Delta_centers = [perr[0]]*nb
Delta_amp = perr[1:nb+1]
Delta_lw  = perr[nb+1:]
Delta_gw  = [0]*nb
## all infos
MINIMAs = [np.min(MODELS[i]) for i in range(nb)]

SNRs = np.abs(np.array(MINIMAs))/noise

FWHMs = [fwhm_fct(opt_lw[i], opt_gw[i]) for i in range(nb)]

FWHM_ERRs = [fwhm_voigt_err(Delta_lw[i], Delta_gw[i], opt_lw[i], opt_gw[i]) for i in range(nb)]

GAUSS_FRACTIONs = [opt_gw[i]/(opt_gw[i] + opt_lw[i]) for i in range(nb)]

LINE_AREAs = [v_to_f(voigt_area(MINIMAs[i], opt_lw[i], opt_gw[i]), freq_band).to(u.Hz)]

# Write Tau, Signal and rms in a dictionary

d_1 = {'Band':band, 'freq_band':freq_band*u.MHz, 'Line':myLine, "velocity": velocity, "final_line": final_line, "residuals": residuals, 
'velocities':velocities, 'velo_0': velos_['velo_0'], 'popt':popt, 'pcov':pcov, 
'n_quantum_moy':n_quantum_moy, 'n_quantum_min':n_quantum_min, 'n_quantum_max':n_quantum_max,
'SNR': SNRs, 'LINE_AREA' : LINE_AREAs, 'GAUSS_FRACTION': GAUSS_FRACTIONs, 'FWHM':np.array(FWHMs)*u.km/u.s, 'FWHM_ERR':np.array(FWHM_ERRs)*u.km/u.s, 
'FWHM_freq':abs(v_to_f(np.array(FWHMs), f0)), 'FWHM_ERR_freq':abs(v_to_f(np.array(FWHM_ERRs), f0)), 
'FWHM_gauss':np.array(opt_gw)*u.km/u.s, 'FWHM_gauss_err':np.array(Delta_gw)*u.km/u.s, 'FWHM_gauss_freq':abs(v_to_f(np.array(opt_gw),freq_band)), 
'FWHM_gauss_freq_err':abs(v_to_f(np.array(Delta_gw), freq_band))}

with open(parent + f"/STACKING/pickles/from-subband{band_inf}-to-{band_sup}-stack-{myLine}-lane-{Lane}-file-{global_path.split('/')[-1]}.pickle", "wb") as output_file:
	pickle.dump(d_1, output_file)